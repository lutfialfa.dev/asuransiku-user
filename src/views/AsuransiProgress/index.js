import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Grid,
  Menu,
  MenuItem,
  ListItemText,
  Typography,
  Card,
  Avatar,
  CardHeader,
  CardContent,
  Divider
} from '@material-ui/core';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import { GenericMoreButton } from 'components';
import PerfectScrollbar from 'react-perfect-scrollbar';

import axios from 'utils/axios';
import { Paginate } from 'components';
import { AsuransiProgress } from 'components';
import { Page } from 'components';

const useStyles = makeStyles(theme => ({
    root: {
        // width: theme.breakpoints.values.lg,
        maxWidth: '100%',
        margin: '0 auto',
        padding: theme.spacing(3)
    },
  header: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    marginBottom: theme.spacing(2)
  },
  title: {
    position: 'relative',
    '&:after': {
      position: 'absolute',
      bottom: -8,
      left: 0,
      content: '" "',
      height: 3,
      width: 48,
      backgroundColor: theme.palette.primary.main
    }
  },
  actions: {
    display: 'flex',
    alignItems: 'center'
  },
  sortButton: {
    textTransform: 'none',
    letterSpacing: 0,
    marginRight: theme.spacing(2)
  },
  paginate: {
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'center'
  },
  container: {
    marginTop: theme.spacing(3)
  },
  rootCard: {
    color: theme.palette.white,
    backgroundColor: "#afeffe",
    padding: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    height : '150px'
  },
  inner: {
    height: 375,
    minWidth: 500
  },
  content: {
    height: '100%'
  },
  cardHeader : {
    backgroundColor : "#2e39c7",
    color: theme.palette.white,
  },
  title: {
    color: theme.palette.white,
  },
  buttonTambah: {
    backgroundColor: '#ea575e',
    color: theme.palette.white,
    '&:hover': {
      backgroundColor: '#ea575e',
    },
  }
}));

const data = {
  value: '25.50',
  currency: '$'
};

const Projects = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const sortRef = useRef(null);
  const [openSort, setOpenSort] = useState(false);
  const [selectedSort, setSelectedSort] = useState('Most popular');
  const [mode, setMode] = useState('grid');
  const [projects, setProjects] = useState([]);

  useEffect(() => {
    let mounted = true;

    const fetchProjects = () => {
      axios.get('/api/projects').then(response => {
        if (mounted) {
          setProjects(response.data.projects);
        }
      });
    };

    fetchProjects();

    return () => {
      mounted = false;
    };
  }, []);

  const handleSortOpen = () => {
    setOpenSort(true);
  };

  const handleSortClose = () => {
    setOpenSort(false);
  };

  const handleSortSelect = value => {
    setSelectedSort(value);
    setOpenSort(false);
  };

  const handleModeChange = (event, value) => {
    setMode(value);
  };

  return (
    <Page
        className={classes.root}
        title="Projects List"
    >
      <Grid
        className={classes.container}
        container
        spacing={3}
      >
        <Grid
          item
          lg={12}
          sm={12}
          xs={12}
        >
          <Card
           {...rest}
           className={clsx(classes.rootCard, className)}
          >
            <div>
              <Typography
                color="inherit"
                component="h3"
                gutterBottom
                variant="overline"
              >
                Roi per customer
              </Typography>
              <div className={classes.details}>
                <Typography
                  color="inherit"
                  variant="h3"
                >
                  {data.currency}
                  {data.value}
                </Typography>
              </div>
            </div>
            {/* <Avatar
              className={classes.avatar}
              color="inherit"
            >
              <AttachMoneyIcon />
            </Avatar> */}
            <Grid item>
              <Button
                // color="secondary"
                variant="contained"
                className={classes.buttonTambah}
              >
                + Tambah Kapal
              </Button>
            </Grid>
          </Card>
        </Grid>

        <Grid
          item
          lg={12}
          sm={12}
          xs={12}
        >
          <Card>
            <CardHeader
              classes={{
                title: classes.title,
              }}
              title=" Asuransi in Progress"
              className={classes.cardHeader}
              style={{color : '#FFFFF'}}
            />
            <Divider />
            <CardContent>
              <div className={classes.content}>
                <Grid
                    container
                    spacing={3}
                >
                    {projects.map(project => (
                    <Grid
                        item
                        key={project.id}
                        md={mode === 'grid' ? 4 : 12}
                        sm={mode === 'grid' ? 6 : 12}
                        xs={12}
                    >
                        <AsuransiProgress project={project} />
                    </Grid>
                    ))}
                </Grid>
                {/* <div className={classes.paginate}>
                    <Paginate pageCount={3} />
                </div> */}
              </div>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      
    </Page>
  );
};

Projects.propTypes = {
  className: PropTypes.string
};

export default Projects;
