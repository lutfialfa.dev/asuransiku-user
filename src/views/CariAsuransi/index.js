import React, { useState, useRef, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
    Button,
    Grid,
    Menu,
    MenuItem,
    ListItemText,
    Typography,
    Card,
    Avatar,
    CardHeader,
    CardContent,
    Divider,
    List,
    ListItem,
    Table,
    TableContainer,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Link
} from '@material-ui/core';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import { GenericMoreButton } from 'components';
import PerfectScrollbar from 'react-perfect-scrollbar';
import moment from 'moment';
import axios from 'utils/axios';
import { Paginate } from 'components';
import { AsuransiProgress } from 'components';
import { Page } from 'components';
import { Label } from 'components';
import Paper from '@material-ui/core/Paper';
import { useDropzone } from 'react-dropzone';

const useStyles = makeStyles(theme => ({
    root: {
        // width: theme.breakpoints.values.lg,
        maxWidth: '100%',
        margin: '0 auto',
        padding: theme.spacing(3)
    },
    dropZone: {
        border: `1px dashed ${theme.palette.divider}`,
        padding: theme.spacing(6),
        outline: 'none',
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        alignItems: 'center',
        '&:hover': {
          backgroundColor: '#FFFFF',
          opacity: 0.5,
          cursor: 'pointer'
        }
    },
    dragActive: {
        backgroundColor: '#FFFFF',
        opacity: 0.5
      },
      image: {
        width: 130
      },
      info: {
        marginTop: theme.spacing(1)
      },
      list: {
        maxHeight: 320
      },
      actions: {
        marginTop: theme.spacing(2),
        display: 'flex',
        justifyContent: 'flex-end',
        '& > * + *': {
          marginLeft: theme.spacing(2)
        }
      }
}));

const data = {
  value: '25.50',
  currency: '$'
};
function createData(name, des,) {
    return { name, des};
  }
const rows = [
    createData('Nama Tertanggung ', 'PT. CDEGHI'),
    createData('Alamat Tertanggung', 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit'),
    createData('Alamat Surat Menyurat', 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit'),
    createData('Ukuran', '2000Kg'),
    createData('Tahun Pembuatan', '2000'),
    createData('Usia Kapal', '21 Tahun'),
    createData('Badan Klasifikasi:', 'Rp1.000.000.000'),
    createData('Badan Klasifikasi:', 'Nemo enim ipsam voluptatem '),
  ];

const CariAsuransi = props => {
  const { project, className, ...rest } = props;

  const classes = useStyles();
  const sortRef = useRef(null);
  const [openSort, setOpenSort] = useState(false);
  const [selectedSort, setSelectedSort] = useState('Most popular');
  const [mode, setMode] = useState('grid');
  const [projects, setProjects] = useState([]);

  useEffect(() => {
    let mounted = true;

    const fetchProjects = () => {
      axios.get('/api/projects').then(response => {
        if (mounted) {
          setProjects(response.data.projects);
        }
      });
    };

    fetchProjects();

    return () => {
      mounted = false;
    };
  }, []);

  const handleSortOpen = () => {
    setOpenSort(true);
  };

  const handleSortClose = () => {
    setOpenSort(false);
  };

  const handleSortSelect = value => {
    setSelectedSort(value);
    setOpenSort(false);
  };

  const handleModeChange = (event, value) => {
    setMode(value);
  };

  const [files, setFiles] = useState([]);

  const handleDrop = useCallback(acceptedFiles => {
    setFiles(files => [...files].concat(acceptedFiles));
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop: handleDrop
  });

  return (
    <Page
        className={classes.root}
        title="Projects List"
    >
      <Grid
        className={classes.files}
        container
        spacing={3}
      >
        {files.map(file => (
          <Grid
            item
            key={file.id}
            lg={4}
            md={4}
            sm={6}
            xs={12}
          >
            {/* <FileCard file={file} /> */}
          </Grid>
        ))}
      </Grid>
      
    </Page>
  );
};

CariAsuransi.propTypes = {
  className: PropTypes.string
};

export default CariAsuransi;
