import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
    Button,
    Grid,
    Menu,
    MenuItem,
    ListItemText,
    Typography,
    Card,
    Avatar,
    CardHeader,
    CardContent,
    Divider,
    List,
    ListItem,
    Table,
    TableContainer,
    TableHead,
    TableRow,
    TableCell,
    TableBody
} from '@material-ui/core';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import { GenericMoreButton } from 'components';
import PerfectScrollbar from 'react-perfect-scrollbar';
import moment from 'moment';
import axios from 'utils/axios';
import { Paginate } from 'components';
import { AsuransiProgress } from 'components';
import { Page } from 'components';
import { Label } from 'components';
import Paper from '@material-ui/core/Paper';
const useStyles = makeStyles(theme => ({
    root: {
        // width: theme.breakpoints.values.lg,
        maxWidth: '100%',
        margin: '0 auto',
        padding: theme.spacing(3)
    },
    header: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        marginBottom: theme.spacing(2)
    },
    title: {
        position: 'relative',
        '&:after': {
        position: 'absolute',
        bottom: -8,
        left: 0,
        content: '" "',
        height: 3,
        width: 48,
        backgroundColor: theme.palette.primary.main
        }
    },
    actions: {
        display: 'flex',
        alignItems: 'center'
    },
    sortButton: {
        textTransform: 'none',
        letterSpacing: 0,
        marginRight: theme.spacing(2)
    },
    paginate: {
        marginTop: theme.spacing(3),
        display: 'flex',
        justifyContent: 'center'
    },
    container: {
        marginTop: theme.spacing(3)
    },
    rootCard: {
        color: theme.palette.white,
        backgroundColor: "#afeffe",
        padding: theme.spacing(3),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        height : '150px'
    },
    inner: {
        height: 375,
        minWidth: 500
    },
    content: {
        height: '100%'
    },
    cardHeader : {
        backgroundColor : "#2e39c7",
        color: theme.palette.white,
    },
    title: {
        color: theme.palette.white,
    },
    buttonTambah: {
        backgroundColor: '#ea575e',
        color: theme.palette.white,
        '&:hover': {
        backgroundColor: '#ea575e',
        },
    },
    listItem: {
        padding: theme.spacing(2, 0),
        justifyContent: 'space-between'
    },
    table: {
        minWidth: 650,
        // maxWidth: 750
    },
}));

const data = {
  value: '25.50',
  currency: '$'
};
function createData(name, des,) {
    return { name, des};
  }
const rows = [
    createData('Nama Tertanggung ', 'PT. CDEGHI'),
    createData('Alamat Tertanggung', 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit'),
    createData('Alamat Surat Menyurat', 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit'),
    createData('Ukuran', '2000Kg'),
    createData('Tahun Pembuatan', '2000'),
    createData('Usia Kapal', '21 Tahun'),
    createData('Badan Klasifikasi:', 'Rp1.000.000.000'),
    createData('Badan Klasifikasi:', 'Nemo enim ipsam voluptatem '),
  ];

const Asuransiku = props => {
  const { project, className, ...rest } = props;

  const classes = useStyles();
  const sortRef = useRef(null);
  const [openSort, setOpenSort] = useState(false);
  const [selectedSort, setSelectedSort] = useState('Most popular');
  const [mode, setMode] = useState('grid');
  const [projects, setProjects] = useState([]);

  useEffect(() => {
    let mounted = true;

    const fetchProjects = () => {
      axios.get('/api/projects').then(response => {
        if (mounted) {
          setProjects(response.data.projects);
        }
      });
    };

    fetchProjects();

    return () => {
      mounted = false;
    };
  }, []);

  const handleSortOpen = () => {
    setOpenSort(true);
  };

  const handleSortClose = () => {
    setOpenSort(false);
  };

  const handleSortSelect = value => {
    setSelectedSort(value);
    setOpenSort(false);
  };

  const handleModeChange = (event, value) => {
    setMode(value);
  };

  return (
    <Page
        className={classes.root}
        title="Projects List"
    >
      <Grid
        className={classes.container}
        container
        spacing={3}
      >
        <Grid
          item
          lg={12}
          sm={12}
          xs={12}
        >
          <Card>
            <CardHeader
              classes={{
                title: classes.title,
              }}
              title=" Detail Kapal"
              className={classes.cardHeader}
              style={{color : '#FFFFF'}}
            />
            <Divider />
            <CardContent>
              <div className={classes.content}>

                <Table className={classes.table} >
                    <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.name}>
                        <TableCell component="th" scope="row">
                            {row.name}
                        </TableCell>
                        <TableCell align="left">{row.des}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                
              </div>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      
    </Page>
  );
};

Asuransiku.propTypes = {
  className: PropTypes.string
};

export default Asuransiku;
