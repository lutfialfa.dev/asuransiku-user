import React, { Component } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import {
  Card,
  CardContent,
  CardMedia,
  Typography,
  Divider,
  Link,
  Avatar,
  Grid,
  Box
} from '@material-ui/core';
import LockIcon from '@material-ui/icons/Lock';
import { Page } from 'components';
import {
  ProfileDetails,
  GeneralSettings
} from '../Settings/components/General/components';

class LandingContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      errors: {},
      email: '',
      password: '',
      persistence: false,
      showPassword: false,
      bukantoken: '',
      username: ''
    };
  }

  render() {
    const classes = this.props.classes;
    return (
      <Page
        // className={classes.root}
        title="Landing">
        <Grid container spacing={3}>
          <Grid item lg={12} sm={12} xs={12}>
            <CardContent
              className={classes.content}
              title="Cover"
              style={{
                height: '600px',
                backgroundImage: 'url("/images/bg.svg")',
                backgroundRepeat: 'no-repeat',
                backgroundSize: '50%',
                backgroundPosition: 'right'
              }}>
              {/* <LockIcon className={classes.icon} /> */}
              <Box ml={5}>
                <img
                  alt="Person"
                  // className={classes.avatar}
                  src="/images/logos/logo-color.png"
                  style={{ maxWidth: '20%' }}
                />
              </Box>
              <Box m={5}>
                <Grid item lg={6} sm={6} xs={6}>
                  <Typography gutterBottom variant="h1">
                    The First, Digitalize, <br></br>
                    and Leading Marine<br></br>
                    Insurance - Related Business Platform
                  </Typography>
                  <Typography variant="subtitle2">
                    AsuransiKapalku.com is a breakthrough platform which allow
                    the digital innovation facilitate the players, as well as
                    the meetings of supply and demand within Marine Insurance
                    Industry.
                  </Typography>
                  {/* <Divider className={classes.divider} /> */}
                  <Link
                    align="center"
                    color="secondary"
                    component={RouterLink}
                    to="/auth/register"
                    underline="always"
                    variant="subtitle2">
                    Don't have an account?
                  </Link>
                </Grid>
              </Box>
            </CardContent>
          </Grid>

          <Grid item lg={12} sm={12} xs={12}>
            <CardContent className={classes.content}>
              {/* <LockIcon className={classes.icon} /> */}
              <Grid className={classes.container} container spacing={3}>
                <Grid
                  item
                  lg={2}
                  sm={12}
                  xs={12}
                  containerjustify="center"
                  alignItems="center"></Grid>
                <Grid
                  item
                  lg={2}
                  sm={6}
                  xs={12}
                  container
                  justify="center"
                  alignItems="center">
                  <img
                    alt="Person"
                    // className={classes.avatar}
                    src="/images/logos/brand-01.png"
                    style={{ maxWidth: '50%' }}
                  />
                </Grid>
                <Grid
                  item
                  lg={2}
                  sm={6}
                  xs={12}
                  container
                  justify="center"
                  alignItems="center">
                  <img
                    alt="Person"
                    // className={classes.avatar}
                    src="/images/logos/brand-02.png"
                    style={{ maxWidth: '50%' }}
                  />
                </Grid>
                <Grid
                  item
                  lg={2}
                  sm={6}
                  xs={12}
                  container
                  justify="center"
                  alignItems="center">
                  <img
                    alt="Person"
                    // className={classes.avatar}
                    src="/images/logos/brand-03.png"
                    style={{ maxWidth: '50%' }}
                  />
                </Grid>
                <Grid
                  item
                  lg={2}
                  sm={6}
                  xs={12}
                  container
                  justify="center"
                  alignItems="center">
                  <img
                    alt="Person"
                    // className={classes.avatar}
                    src="/images/logos/brand-04.png"
                    style={{ maxWidth: '50%' }}
                  />
                </Grid>
                <Grid
                  item
                  lg={2}
                  sm={4}
                  xs={6}
                  container
                  justify="center"
                  alignItems="center"></Grid>
              </Grid>
            </CardContent>
          </Grid>

          <Grid className={classes.container} container>
            <Grid
              item
              lg={6}
              sm={6}
              xs={12}
              style={{ backgroundColor: '#f9fafb', padding: '50px' }}>
              <Typography gutterBottom variant="h1">
                Nemo enim ipsam <br></br>
                voluptatem quia voluptas <br></br>
                sit aspernatur
              </Typography>
              <Typography variant="subtitle2">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, ed do
                eiusmod tempor incididunt ut labore et dolore.
              </Typography>
              {/* <Divider className={classes.divider} /> */}
              <Link
                align="center"
                color="secondary"
                component={RouterLink}
                to="/auth/register"
                underline="always"
                variant="subtitle2">
                Don't have an account?
              </Link>
            </Grid>
            <Grid
              item
              lg={6}
              sm={6}
              xs={12}
              container 
              style={{ backgroundColor: '#2e39c7' }}>
              <Grid item lg={6} md={6} xl={6} xs={12}  container justify="center" alignItems="center">
                <ProfileDetails profile="" style={{ height:'442px',borderRadius: '5px' , margin: '25px'}}/>
              </Grid>
              <Grid item lg={6} md={6} xl={6} xs={12} container justify="center" alignItems="center">
              <ProfileDetails profile="" style={{ height:'442px',borderRadius: '5px' , margin: '25px'}}/>
              </Grid>
              <Grid item lg={6} md={6} xl={6} xs={12} container justify="center" alignItems="center">
              <ProfileDetails profile="" style={{ height:'442px',borderRadius: '5px' , margin: '25px'}}/>
              </Grid>
              <Grid item lg={6} md={6} xl={6} xs={12} container justify="center" alignItems="center">
              <ProfileDetails profile="" style={{ height:'442px',borderRadius: '5px' , margin: '25px'}}/>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        
        {/* <Grid className={classes.container} container style={{ backgroundColor: '#252525', padding: '50px' }}>
          <Grid
            item
            lg={2}
            sm={4}
            xs={6}
            container
            justify="center"
            alignItems="center">
            <img
              alt="Person"
              // className={classes.avatar}
              src="/images/logos/logo-white.png"
              style={{ maxWidth: '50%' }}
            />
          </Grid>
        </Grid> */}
      </Page>
    );
  }
}

export default LandingContent;
